'use strict';

const obj = {
    a            : 2,
    b            : 3,
    d            : false,
    e            : null,
    f            : undefined,
    [Symbol('a')]: 'a'
};

Object.defineProperty(obj, 'not-enumerable', {
    value: 'not-enumerable',
    enumerable: false
});

// method returns an array of all properties (including non-enumerable properties except for those which use Symbol) found directly upon a given object
const keys = Object.getOwnPropertyNames(obj);

console.log(obj);
console.log(keys.join(', '));
