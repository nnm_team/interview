'use strict';

const obj = {
    a            : 2,
    b            : 3,
    d            : false,
    e            : null,
    f            : undefined,
    [Symbol('a')]: 'a'
};

Object.defineProperty(obj, 'not-enumerable', {
    value: 'not-enumerable',
    enumerable: false
});

// method returns an array of a given object's own property names, in the same order as we get with a normal loop
const keys = Object.keys(obj);

console.log(keys.join(', '));
