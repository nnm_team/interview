'use strict';

const obj = {
    a            : 2,
    b            : 3,
    d            : false,
    e            : null,
    f            : undefined,
    [Symbol.for('a')]: 'a'
};

Object.defineProperty(obj, 'not-enumerable', {
    value     : 'not-enumerable',
    enumerable: false
});

console.log('a' in obj);
console.log(Symbol.for('a') in obj);
console.log('not-enumerable' in obj);

console.log('toString' in obj); // work for properties from prototypes chain too
