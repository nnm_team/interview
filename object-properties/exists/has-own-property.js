'use strict';

const obj = {
    a            : 2,
    b            : 3,
    d            : false,
    e            : null,
    f            : undefined,
    [Symbol('a')]: 'a'
};

Object.defineProperty(obj, 'not-enumerable', {
    value: 'not-enumerable',
    enumerable: false
});

// method returns a boolean indicating whether the object has the specified property as its own property (as opposed to inheriting it).
// work with not enumerable fields too

console.log(obj.hasOwnProperty('a'));
console.log(obj.hasOwnProperty('d'));
console.log(obj.hasOwnProperty('e'));
console.log(obj.hasOwnProperty('f'));
console.log(obj.hasOwnProperty('not-enumerable'));

console.log(obj.hasOwnProperty('ff'));
console.log(obj.hasOwnProperty('toString'));
