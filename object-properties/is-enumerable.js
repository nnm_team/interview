'use strict';

const obj = {
    a            : 2,
    b            : 3,
    d            : false,
    e            : null,
    f            : undefined,
    [Symbol.for('a')]: 'a'
};

Object.defineProperty(obj, 'not-enumerable', {
    value: 'not-enumerable',
    enumerable: false
});

console.log(obj.propertyIsEnumerable);
console.log(obj.__proto__.propertyIsEnumerable);
console.log(Object.prototype.propertyIsEnumerable);

console.log(obj.propertyIsEnumerable('a'));
console.log(obj.propertyIsEnumerable(Symbol.for('a')));
console.log(obj.propertyIsEnumerable('not-enumerable'));
