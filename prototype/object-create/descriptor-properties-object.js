'use strict';

const Obj = {
    a: 2
};

// object-descriptor
const propertiesObject = {
    y: {
        value: 20
    }
};

const obj = Object.create(Obj, propertiesObject);

console.log(obj.a);
console.log(obj.y);
console.log(obj.__proto__ === Obj);
console.log(Obj.__proto__ === Object.prototype);

