'use strict';

const Obj = {
    a: 2
};

const obj = Object.create(Obj);

console.log(obj.a);
console.log(Obj.prototype);
console.log(obj.__proto__ === Obj);
console.log(Obj.__proto__ === Object.prototype);

