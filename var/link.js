'use strict';

const a = {};
const b = {};

console.log(a);
console.log(b);

console.log(a.toString());
console.log(b.toString());

console.log(a == b);
console.log(a === b);
console.log(a.toString() === b.toString());

console.log(a === a);
