'use strict';

function* f() {
    const a = yield 2;

    const b = yield (a + 3);

    console.log(`a = ${a}`);
    console.log(`b = ${b}`);
}

const generator = f();

const a = generator.next().value;
const b = generator.next(a).value;

generator.next(b);
