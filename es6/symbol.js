'use strict';

const a = Symbol('a');

const obj = {
    [a]              : 'a value',
    [Symbol.for('b')]: 'b value'
};

console.log(obj.a);
console.log(obj[a]);
console.log(obj[Symbol.for('b')]);
console.log(obj.hasOwnProperty(a));

const symbols = Object.getOwnPropertySymbols(obj);

symbols.forEach(console.log);

const c = Symbol.for('c');

console.log(c === Symbol('c'));
console.log(c === Symbol.for('c'));