'use strict';

const arr = [1, 2, 3];

const iterator = arr[Symbol.iterator]();

for (let value, current = iterator.next(); !current.done; current = iterator.next()) {
    value = current.value;

    console.log(value);
}
