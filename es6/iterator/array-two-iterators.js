'use strict';

const arr = [1, 2];

const iterator1 = arr[Symbol.iterator]();
const iterator2 = arr[Symbol.iterator]();

console.log(iterator1.next()); // { value: 1, done: false }
console.log(iterator1.next()); // { value: 2, done: false }
console.log(iterator1.next()); // { value: undefined, done: true }

console.log(iterator2.next()); // { value: 1, done: false }
console.log(iterator2.next()); // { value: 2, done: false }
console.log(iterator2.next()); // { value: undefined, done: true }
