'use strict';

const obj = {

    data: [1, 2, 3],

    [Symbol.iterator]() {
        let index = 0;
        const self = this;

        return {

            next() {
                const hasValue = self.data.hasOwnProperty(index); //

                if (hasValue) {
                    return {value: self.data[index++], done: false};
                }

                return {value: undefined, done: true};
            }

        }
    }

};

const iterator = obj[Symbol.iterator]();

console.log(iterator.next()); // { value: 1, done: false }
console.log(iterator.next()); // { value: 2, done: false }
console.log(iterator.next()); // { value: 3, done: false }

console.log(iterator.next()); // { value: undefined, done: true }
