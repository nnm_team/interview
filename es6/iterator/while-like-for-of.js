'use strict';

const arr = [1, 2, 3];

const iterator = arr[Symbol.iterator]();

let current = iterator.next();

while (!current.done) {
    const value = current.value;

    console.log(value);

    current = iterator.next();
}
