'use strict';

const COUNT = 10000;

const array = [];

for (let i = 0; i < COUNT; i++) {
    array.push(i);
}

let sum = 0;

console.time('for');

for (let i = 0, length = array.length; i < length; i++) {
    sum += array[i];
}

console.log(`sum = ${sum}`);
console.timeEnd('for');

console.time('for-iterator');

sum = 0;

const iterator1 = array[Symbol.iterator]();

for (let value, current = iterator1.next(); !current.done; current = iterator1.next()) {
    value = current.value;

    sum += value;
}

console.log(`sum = ${sum}`);
console.timeEnd('for-iterator');

console.time('while-iterator');

sum = 0;

const iterator2 = array[Symbol.iterator]();

let current = iterator2.next();

while (!current.done) {
    const value = current.value;

    sum += value;

    current = iterator2.next();
}

console.log(`sum = ${sum}`);
console.timeEnd('while-iterator');

console.time('for-of');

sum = 0;

for (const item of array) {
    sum += item;
}

console.log(`sum = ${sum}`);
console.timeEnd('for-of');
