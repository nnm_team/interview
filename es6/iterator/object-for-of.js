'use strict';

const obj = {

    data: [1, 2, 3],

    [Symbol.iterator]() {
        let index = 0;
        const self = this;

        return {

            next() {
                const hasValue = self.data.hasOwnProperty(index);

                if (hasValue) {
                    return {value: self.data[index++], done: false};
                }

                return {value: undefined, done: true};
            }

        }
    }

};

for (const item of obj) {
    console.log(item);
}
