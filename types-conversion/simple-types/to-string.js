'use strict';

console.log('undefined to string:');
console.log('\tString(undefined): ' + String(undefined));
console.log('\t(\'\' + undefined): ' + ('' + undefined));
console.log('\ttypeof (\'\' + undefined): ' + typeof ('' + undefined));

console.log('null to string:');
console.log('\tString(null): ' + String(null));
console.log('\t(\'\' + null): ' + ('' + null));
console.log('\ttypeof (\'\' + null): ' + typeof ('' + null));

console.log('boolean to string:');
console.log('\tString(true): ' + String(true));
console.log('\t(\'\' + true): ' + ('' + true));
console.log('\tString(false): ' + String(false));
console.log('\t(\'\' + false): ' + ('' + false));

console.log('number to string:');
console.log('\tString(0): ' + String(0));
console.log('\tString(0): ' + String(+0));
console.log('\tString(-0): ' + String(-0));
console.log('\tString(1): ' + String(1));
console.log('\tString(-1): ' + String(-1));
console.log('\tString(NaN): ' + String(NaN));
console.log('\tString(Infinity): ' + String(Infinity));
console.log('\tString(+Infinity): ' + String(+Infinity));
console.log('\tString(-Infinity): ' + String(-Infinity));
