'use strict';

console.log('undefined to number:');
console.log('\tNumber(undefined): ' + Number(undefined));
console.log('\t(+undefined): ' + (+undefined));

console.log('null to number:');
console.log('\tNumber(null): ' + Number(null));
console.log('\t(+null): ' + (+null));

console.log('boolean to number:');
console.log('\tNumber(true): ' + Number(true));
console.log('\t(+true): ' + (+true));
console.log('\tNumber(false): ' + Number(false));
console.log('\t(+false): ' + (+false));

console.log('string to number:');
console.log('\tNumber(\'\'): ' + Number(''));
console.log('\t(+\'\'): ' + (+''));
console.log('\tNumber(\'1\'): ' + Number('1'));
console.log('\t(+\'1\'): ' + (+'1'));
console.log('\tNumber(\'1.2\'): ' + Number('1.2'));
console.log('\t(+\'1.2\'): ' + (+'1.2'));
console.log('\tNumber(\'string\'): ' + Number('string'));
console.log('\t(+\'string\'): ' + (+'string'));
console.log('\tNumber(\'Infinity\'): ' + Number('Infinity'));
console.log('\t(+\'Infinity\'): ' + (+'Infinity'));
console.log('\tNumber(\'NaN\'): ' + Number('NaN'));
console.log('\t(+\'NaN\'): ' + (+'NaN'));
