'use strict';

console.log('undefined to boolean:');
console.log('\tBoolean(undefined): ' + Boolean(undefined));
console.log('\t(!!undefined): ' + (!!undefined));

console.log('null to boolean:');
console.log('\tBoolean(null): ' + Boolean(null));
console.log('\t(!!null): ' + (!!null));

console.log('string to boolean:');
console.log('\tBoolean(\'\'): ' + Boolean(''));
console.log('\t(!!\'\'): ' + (!!''));
console.log('\tBoolean(\'1\'): ' + Boolean('1'));
console.log('\t(!!\'1\'): ' + (+'1'));
console.log('\tBoolean(\'1.2\'): ' + Boolean('1.2'));
console.log('\t(!!\'1.2\'): ' + (!!'1.2'));
console.log('\tBoolean(\'string\'): ' + Boolean('string'));
console.log('\t(!!\'string\'): ' + (!!'string'));
console.log('\tBoolean(\'Infinity\'): ' + Boolean('Infinity'));
console.log('\t(!!\'Infinity\'): ' + (!!'Infinity'));
console.log('\tBoolean(\'NaN\'): ' + Boolean('NaN'));
console.log('\t(!!\'NaN\'): ' + (!!'NaN'));

console.log('number to boolean:');
console.log('\tBoolean(0): ' + Boolean(0));
console.log('\tBoolean(0): ' + Boolean(+0));
console.log('\tBoolean(-0): ' + Boolean(-0));
console.log('\tBoolean(1): ' + Boolean(1));
console.log('\tBoolean(-1): ' + Boolean(-1));
console.log('\tBoolean(NaN): ' + Boolean(NaN));
console.log('\tBoolean(Infinity): ' + Boolean(Infinity));
console.log('\tBoolean(+Infinity): ' + Boolean(+Infinity));
console.log('\tBoolean(-Infinity): ' + Boolean(-Infinity));
