'use strict';

const objWithToString = {
    a: 'a',

    toString: function () {
        return this.a + '-to-string';
    }
};

const objWithValueOfAndToString = {
    a: 'a',
    b: 'b',

    toString: function () {
        return this.a + '-to-string';
    },

    valueOf: function () {
        return this.a + '-to-string';
    }
};

const objWithValueOf = {
    a: 'a',

    valueOf: function () {
        return this.a + '-to-string';
    }
};

const objWithoutToStringButWithValueOf = {
    a: 'a',

    valueOf: function () {
        return this.a + '-to-string';
    }
};

console.log('empty object to string');
console.log('\tString({}): ' + String({}));
console.log('\t\'\' + {}: ' + ('' + {}));

console.log('object with custom to-string method to string');
console.log('\tString(objWithToString): ' + String(objWithToString));
console.log('\t\'\' + objWithToString: ' + ('' + objWithToString));

console.log('object with custom to-string & value-of methods to string');
console.log('\tString(objWithValueOfAndToString): ' + String(objWithValueOfAndToString));
console.log('\t\'\' + objWithValueOfAndToString: ' + ('' + objWithValueOfAndToString));

console.log('object with value-of method to string');
console.log('\tString(objWithValueOf): ' + String(objWithValueOf));
console.log('\t\'\' + objWithValueOf: ' + ('' + objWithValueOf));

delete Object.prototype.toString;

console.log('object without custom to-string | with value-of method to string');
console.log('\tString(objWithoutToStringButWithValueOf): ' + String(objWithoutToStringButWithValueOf));
console.log('\t\'\' + objWithoutToStringButWithValueOf: ' + ('' + objWithoutToStringButWithValueOf));