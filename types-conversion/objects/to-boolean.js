'use strict';

// Conversion any object to boolean always return true.

console.log(!!{}); // true
console.log(Boolean({})); // true
console.log(!![]); // true
console.log(Boolean([])); // true
console.log(!!{a: 1}); // true
console.log(Boolean({a: 1})); // true
console.log(!![1]); // true
console.log(Boolean([1])); // true
console.log(!!function () {}); // true
console.log(Boolean(function () {})); // true
