'use strict';

const objWithValueOf = {
    a: 'a',

    valueOf: function () {
        return 888;
    }
};

const objWithoutValueOfToStringString = {
    a: 'a',

    toString: function () {
        return 'string';
    }
};

const objWithoutValueOfToStringNumber = {
    a: 'a',

    toString: function () {
        return 888;
    }
};

const objWithoutValueOfToStringStringNumber = {
    a: 'a',

    toString: function () {
        return '888';
    }
};

console.log('empty object to string');
console.log('\tNumber({}): ' + Number({}));
console.log('\t+{}: ' + (+{}));

console.log('object without value of (to string return string) to string');
console.log('\tNumber(objWithoutValueOfToStringString): ' + Number(objWithoutValueOfToStringString));
console.log('\t+objWithoutValueOfToStringString: ' + (+objWithoutValueOfToStringString));

console.log('object without value of (to string return number) to string');
console.log('\tNumber(objWithoutValueOfToStringNumber): ' + Number(objWithoutValueOfToStringNumber));
console.log('\t+objWithoutValueOfToStringNumber: ' + (+objWithoutValueOfToStringNumber));

console.log('object without value of (to string return number) to string');
console.log('\tNumber(objWithoutValueOfToStringStringNumber): ' + Number(objWithoutValueOfToStringStringNumber));
console.log('\t+objWithoutValueOfToStringStringNumber: ' + (+objWithoutValueOfToStringStringNumber));
