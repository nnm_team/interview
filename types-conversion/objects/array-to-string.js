'use strict';

console.log('[] to string');
console.log('\tString([]): ' + '"' + String([]) + '"');
console.log('\t\'\' + []: ' + '"' + ('' + []) + '"');

console.log('[1] to string');
console.log('\tString([1]): ' + '"' + String([1]) + '"');
console.log('\t\'\' + [1]: ' + '"' + ('' + [1]) + '"');

console.log('[1, 2] to string');
console.log('\tString([1, 2]): ' + '"' + String([1, 2]) + '"');
console.log('\t\'\' + [1, 2]: ' + '"' + ('' + [1, 2]) + '"');

console.log('[\'a\'] to string');
console.log('\tString([\'a\']): ' + '"' + String(['a']) + '"');
console.log('\t\'\' + [\'a\']: ' + '"' + ('' + ['a']) + '"');

console.log('[\'a\', \'b\'] to string');
console.log('\tString([\'a\', \'b\']): ' + '"' + String(['a', 'b']) + '"');
console.log('\t\'\' + [\'a\', \'b\']: ' + '"' + ('' + ['a', 'b']) + '"');
