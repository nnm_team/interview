'use strict';

const obj = {};

obj['a'] = 1;

Object.preventExtensions(obj);

obj.a = 2;

delete obj.a;

try {
    obj['b'] = 1;
} catch (err) {
    console.error(err);
}
