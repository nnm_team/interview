'use strict';

const obj = {};

obj['a'] = 1;

Object.seal(obj);

obj.a = 2;

try {
    delete obj.a;
} catch (err) {
    console.error(err);
}

try {
    obj['b'] = 1;
} catch (err) {
    console.error(err);
}
