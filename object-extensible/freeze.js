'use strict';

const obj = {};

obj['a'] = 1;

Object.freeze(obj);

try {
    obj.a = 2;
} catch (err) {
    console.error(err);
}

try {
    delete obj.a;
} catch (err) {
    console.error(err);
}

try {
    obj['b'] = 1;
} catch (err) {
    console.error(err);
}
