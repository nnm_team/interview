'use strict';

const obj = {
    _c: 3
};

Object.defineProperty(obj, 'a', {
    value       : 1,
    writable    : false,
    enumerable  : true,
    configurable: true
});

Object.defineProperty(obj, 'b', {
    value       : 2,
    writable    : true,
    enumerable  : true,
    configurable: true
});

Object.defineProperty(obj, 'c', {
    get       : function () {
        return this._c;
    },
    set       : function (value) {
        this._c = value;
    },
    enumerable: true
});

console.log(obj.a);
console.log(obj.b);
console.log(obj.c);

obj.b = 3;
obj.c = 5;

try {
    obj.a = 4;
} catch (err) {
    console.error(err);
}

