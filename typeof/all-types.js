'use strict';

console.log(typeof 1);   // number
console.log(typeof 1.2);   // number
console.log(typeof 'string');   // string
console.log(typeof '1.2');   // string
console.log(typeof true);   // boolean
console.log(typeof false);   // boolean
console.log(typeof []);   // object
console.log(typeof {});   // object
console.log(typeof null);   // object
console.log(typeof undefined);   // undefined
