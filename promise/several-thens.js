'use strict';

const promise = new Promise(resolve => resolve(2));

promise.then(value => value * 2)
    .then(value => {
        console.log(value);

        return value;
    });

promise.then(console.error);
