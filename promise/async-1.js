'use strict';

console.log('start');

const promise = new Promise(resolve => {
    console.log('promise root');

    resolve(2);
}).then(value => {
    console.log('first then');

    return value * 2;
});

promise.then(value => {
    console.log('second then', value);

    return value * 3;
});

promise.then(value => {
    console.log('third then', value);

    return value * 4;
});

console.log('end');
