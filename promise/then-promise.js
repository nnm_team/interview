'use strict';

new Promise((resolve => resolve(2)))
    .then(value =>
        new Promise(resolve => resolve(value))
            .then(value => 2 * value))
    .then(console.log);